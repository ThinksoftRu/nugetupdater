﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using NuGet;

namespace Nuget
{
    public class NugetUpdater
    {
        // ReSharper disable InconsistentNaming
        public enum OS
        {
            Windows,
            Linux,
            Mac
        }

        /// <summary>
        /// Raise after Updater.exe started. Current process must be Exit.
        /// </summary>
        public event EventHandler NeedClose;

        /// <summary>
        /// Raise after DownloadAndUnpack method execution
        /// </summary>
        public event PackageDownloadedDelegate VersionDownloaded;

        public delegate void PackageDownloadedDelegate(object sender, Version version);

        private readonly Regex pattern = new Regex(@"(\G\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\Z)");
        private readonly DirectoryInfo _updateRootDir;
        private readonly IPackageRepository _repo;
        private readonly string _updaterRootDir;
        private readonly string _packageId;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="updateRootDir">Folder for updates downloading</param>
        /// <param name="updaterRootDir">Folder for Updater.exe copy</param>
        /// <param name="packageId">Package name</param>
        /// <param name="packegaSource">Nuget server enpoint</param>
        public NugetUpdater(string updateRootDir, string updaterRootDir, string packageId, string packegaSource = "https://packages.nuget.org/api/v2")
        {
            _updateRootDir = new DirectoryInfo(updateRootDir);
            _updaterRootDir = updaterRootDir;
            _packageId = packageId;
            _repo = PackageRepositoryFactory.Default.CreateRepository(packegaSource);

            Cleunup();
        }

        /// <summary>
        /// Get last version from Nuget server
        /// </summary>
        /// <returns></returns>
        public Version GetLastVersion()
        {
            var package = GetLastPackage();
            return package != null ? package.Version.Version : null;
        }

        /// <summary>
        /// Get latest version which store local in updateRootDir
        /// </summary>
        /// <returns></returns>
        public Version GetLastDownloadedVersion()
        {
            var maxVersion =
                _updateRootDir.GetDirectories()
                    .Where(d => pattern.IsMatch(d.Name) && (d.Attributes & FileAttributes.Hidden) == 0)
                    .Select(d => new Version(d.Name))
                    .Max();
            return maxVersion;
        }

        /// <summary>
        /// Download and local unpack package from Nuget server to updateRootDir
        /// </summary>
        /// <param name="version">Version for download. Default last version</param>
        public void DownloadAndUnpack(Version version = null)
        {
            var package = GetPackage(version ?? GetLastVersion());
            var updateDir = _updateRootDir.CreateSubdirectory(package.Version.ToString());
            updateDir.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            package.ExtractContents(new PhysicalFileSystem(_updateRootDir.FullName), updateDir.FullName);
            updateDir.Attributes = FileAttributes.Directory | FileAttributes.Normal;
            OnVersionDownloaded(package.Version.Version);
        }

        /// <summary>
        /// Run Updater for files copy
        /// </summary>
        /// <param name="destinationDir">Folder with old files</param>
        /// <param name="os"></param>
        /// <param name="restartApplicationAfterUpdate">Restart application after updating</param>
        /// <returns></returns>
        public bool Update(string destinationDir, OS os, bool restartApplicationAfterUpdate = true)
        {
            try
            {
                var updaterPath = BuildUpdateAssembly(_updaterRootDir);
                if (string.IsNullOrEmpty(updaterPath))
                    throw new FileNotFoundException("Updater assembly not found.");

                var lastDownloaded = GetLastDownloadedVersion();
                if (lastDownloaded == null)
                    throw new DirectoryNotFoundException();

                var updateDir = _updateRootDir.CreateSubdirectory(lastDownloaded.ToString());

                switch (os)
                {
                    case OS.Windows:
                    {
                        RunUpdater(updaterPath, updateDir.FullName, destinationDir, restartApplicationAfterUpdate);
                        break;
                    }
                    case OS.Linux:
                    {
                        RunLinuxUpdater(updaterPath, updateDir.FullName, destinationDir, restartApplicationAfterUpdate);
                        break;
                    }
                    case OS.Mac:
                    {
                        throw new NotImplementedException();
                    }
                }
                OnNeedClose();
            }

            catch (Exception e)
            {
                using (var fs = File.Open(".\\update.log", FileMode.Append))
                {
                    var text = string.Format("{0} {1}", DateTime.Now.ToString("u"), e.Message);
                    var bytes = System.Text.Encoding.Default.GetBytes(string.Format("{0}\r\n----\r\n{1}\r\n\r\n", text, e.StackTrace));
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
            }
            return true;
        }


        private static string BuildUpdateAssembly(string buildDir)
        {
            var outputAssembly = Path.Combine(buildDir, "Updater.exe");
            var name = Assembly.GetEntryAssembly().GetManifestResourceNames().Single(n => n.ToLower().EndsWith("updater.exe"));
            byte[] data;
            using (var stream = Assembly.GetEntryAssembly().GetManifestResourceStream(name))
            {
                if (stream == null)
                    return null;

                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    data = ms.ToArray();
                    ms.Close();
                }
                stream.Close();
            }
            File.WriteAllBytes(outputAssembly, data);
            return outputAssembly;
        }

        private IPackage GetLastPackage()
        {
            var lastVersion = _repo.FindPackagesById(_packageId).Max(p => p.Version);
            return GetPackage(lastVersion.Version);

        }

        private IPackage GetPackage(Version version)
        {
            return _repo.FindPackagesById(_packageId).FirstOrDefault(p => p.Version.Version == version);
        }

        private static void RunUpdater(string assemblyPath, string updateDir, string destinationDir, bool restartApplicationAfterUpdate)
        {
            var processId = Process.GetCurrentProcess().Id;
            var processFile = restartApplicationAfterUpdate ? Assembly.GetEntryAssembly().Location : "";
            var startInfo = new ProcessStartInfo(assemblyPath, $"{processId} \"{updateDir}\" \"{destinationDir}\" \"{processFile}\"")
            {
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };
            Process.Start(startInfo);
        }

        private static void RunLinuxUpdater(string assemblyPath, string updateDir, string destinationDir, bool restartApplicationAfterUpdate)
        {
            var processId = Process.GetCurrentProcess().Id;
            var processFile = restartApplicationAfterUpdate ? Assembly.GetEntryAssembly().Location : "";
            var startInfo = new ProcessStartInfo("mono",
                $"'{assemblyPath}' {processId} '{updateDir}' '{destinationDir}' '{processFile}'")
            {
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };
            Process.Start(startInfo);
        }

        private void OnNeedClose()
        {
            NeedClose?.Invoke(this, EventArgs.Empty);
        }

        private void OnVersionDownloaded(Version version)
        {
            VersionDownloaded?.Invoke(this, version);
        }

        private void Cleunup()
        {
            try
            {
                foreach (var directoryInfo in _updateRootDir.EnumerateDirectories().Where(d => pattern.IsMatch(d.Name) && (d.Attributes & FileAttributes.Hidden) != 0)
                    )
                    directoryInfo.Delete(true);
            }
            catch
            {
                // ignored
            }
        }
    }
}
