﻿using System;
using System.IO;
using System.Reflection;

namespace NugetUpdater.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Select OS version (differrent execution for Updater exe)
            const Nuget.NugetUpdater.OS os = Nuget.NugetUpdater.OS.Windows;
            
            // For Linux and Mac Current directory set to execution assembly location instead Home directory
            if (os != Nuget.NugetUpdater.OS.Windows)
                Directory.SetCurrentDirectory(new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName);
            
            var canClose = false;

            var updater = new Nuget.NugetUpdater(
                Environment.CurrentDirectory,
                Environment.CurrentDirectory,
                "ScreenshotMonitor",
                "http://nuget.thinksoft.ru/nuget/");

            //Call after Updater.exe process started
            updater.NeedClose += (e, a) => { canClose = true; };

            // Call after DownloadAndUnpack() method executed
            updater.VersionDownloaded += (s, v) =>
            {
                Console.WriteLine("New version available: {0}. Restart application for update", v);
                updater.Update(Environment.CurrentDirectory, os, false);
            };

            // Check local copy for Latest version
            var version = updater.GetLastDownloadedVersion();

            if (version != null)
                updater.Update(Environment.CurrentDirectory, os, false);
            else
            {
                // Check current version. and run downloading if exist new version.
                var myVersion = Assembly.GetEntryAssembly().GetName().Version;
                var serverVersion = updater.GetLastVersion();
                if (myVersion >= serverVersion) return;
                updater.DownloadAndUnpack(serverVersion);
            }

            while (!canClose)
            {
                System.Threading.Thread.Sleep(2000);
            }
        }
    }
}
