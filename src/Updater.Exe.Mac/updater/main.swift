import Foundation
import AppKit


println(Process.arguments.count)

let updaterExe = Process.arguments[0]; //Default parameter
let processId = Process.arguments[1];
let fromDir = Process.arguments[2];
let toDir = Process.arguments[3];
let processFileName = Process.arguments[4];



let filemanager:NSFileManager = NSFileManager()
let files = filemanager.enumeratorAtPath(fromDir)

if(!filemanager.fileExistsAtPath(toDir)) {
    filemanager.createDirectoryAtPath(toDir, withIntermediateDirectories: false, attributes: nil, error: nil);
}

for filename in files! {
    println(filename)

    let fromFile = fromDir + "/" + (filename as! String);
    let toFile = toDir + "/" + (filename as! String);

    if (filemanager.fileExistsAtPath(toFile)) {
        filemanager.removeItemAtPath(toFile, error: nil);
    }

    filemanager.copyItemAtPath(fromFile, toPath: toFile, error: nil);

    if filemanager.isWritableFileAtPath(filename as! String) {
        print(" - File is writable\r\n")
    } else {
        print(" - File is read-only\r\n")
    }
}


filemanager.removeItemAtPath(fromDir, error: nil);

func getRelativeDirName(parentFolder: String, childFolder: String) -> String {
    return childFolder.stringByReplacingOccurrencesOfString(parentFolder, withString: "");
}
