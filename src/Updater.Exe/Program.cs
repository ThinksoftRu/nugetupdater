﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;


namespace Updater
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Updater start...");
                foreach (var s in args)
                    Console.WriteLine(s);

                Update(args);
                Console.WriteLine("Updater end...");
            }
            catch (Exception e)
            {
                using (var fs = File.Open(Path.Combine(args[2], "update.log"), FileMode.Append))
                {
                    var text = $"{DateTime.Now.ToString("u")} {e.Message}";
                    System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
                    var bytes = System.Text.Encoding.UTF8.GetBytes($" 'updater.exe' {text}\r\n----\r\n{e.StackTrace}\r\n\r\n");
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
            }
        }

        private static void Update(IList<string> args)
        {
            var parentProcessId = int.Parse(args[0]);
            var updateDir = args[1];
            var destinationDir = args[2];
            var processFile = args.Count > 3 ? args[3] : null;
            
            if (!ProcessIsRun(parentProcessId))
                CopyUpdate(updateDir, destinationDir);
            else
            {
                for (var i = 0; i < 5; i++)
                {
                    System.Threading.Thread.Sleep(i*500);
                    if (!ProcessIsRun(parentProcessId))
                        break;
                }
                if (ProcessIsRun(parentProcessId))
                    throw new Exception("Process still started");
                CopyUpdate(updateDir, destinationDir);
            }

            if (!string.IsNullOrEmpty(processFile))
                Process.Start(processFile);
        }

        private static void CopyUpdate(string updateDir, string destinationDir)
        {
            var workDi = new DirectoryInfo(destinationDir);
            var updateDi = new DirectoryInfo(updateDir);

            var newFiles = updateDi.GetFiles("*.*", SearchOption.AllDirectories).Select(fi => GetRelativeDirName(updateDir, fi.FullName)).ToArray();
            var existFiles = workDi.GetFiles("*.*", SearchOption.AllDirectories).Select(fi => GetRelativeDirName(destinationDir, fi.FullName)).ToArray();

            var filesIntersection = existFiles.Intersect(newFiles);

            if (filesIntersection.Any(f => !CanUse(Path.Combine(destinationDir, f))))
                throw new AccessViolationException();

            foreach (var file in newFiles)
            {
                var existfileName = Path.Combine(workDi.FullName, file);
                var updateFileName = Path.Combine(updateDi.FullName, file);

                if (File.Exists(existfileName))
                    File.Delete(existfileName);

                var directory = new FileInfo(existfileName).DirectoryName;
                if (directory != null && !Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                File.Copy(updateFileName, existfileName, true);
                Console.WriteLine("File copied: {0}", file);
            }
            Directory.Delete(updateDir, true);
        }


        private static bool ProcessIsRun(int processId)
        {
            try
            {
                Process.GetProcessById(processId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static string GetRelativeDirName(string parentFolder, string childFolder)
        {
            return childFolder.Replace(parentFolder, "").TrimStart('\\');
        }

        private static bool CanUse(string fileName)
        {
            try
            {
                using (var fs = File.OpenWrite(fileName))
                {
                    fs.Close();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
